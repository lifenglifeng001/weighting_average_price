# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class monthly_price(osv.osv):
    _name = 'monthly.price'
    _description = u'月末价格'
    _order = "period_id desc,product_id,location_id"
    _columns = {
        'product_id': fields.many2one('product.product', u'产品', domain=[('calc_ok', '=', True)]),
        'location_id': fields.many2one('stock.location', u'库位', domain=[('type', '=', 'base'), ('calc_ok', '=', True)]),
        'period_id': fields.many2one('account.period', u'会计期间', required=True),
        'qty_in': fields.float(u'本月采购数量'),
        'price_in': fields.float(u'采购均价', digits=(7, 9)),
        'qty_end': fields.float(u'月末库存数量'),
        'price': fields.float(u'月末单价', digits=(7, 9)),
         }
monthly_price()
