# -*- encoding: utf-8 -*-
{
    'name': "采购加权平均",
    'description': "入库结合期初库存进行加权平均的过程",
    'version': '0.1',
    'category': 'Generic Modules/report',
    'depends': ['stock', 'analytic', 'product'],
    'data': [],
    'data': [
                   'base_set_view.xml',
                   'monthly_price_view.xml',
                   'wizard/doing_monthly_view.xml',
                   # 'wizard/report_monthly_view.xml',
                  ],
    'demo_xml': [],
    'test': [],
    'installable': True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
