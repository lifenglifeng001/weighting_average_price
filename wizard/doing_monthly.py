# -*- coding: utf-8 -*- #
import datetime
from datetime import timedelta
import time
from openerp import _

from openerp.osv import fields, osv
import logging

_logger = logging.getLogger(__name__)
class monthly_price_wizard(osv.osv_memory):
    _name = 'monthly.price.wizard'
    _order = "id desc"

    def _get_period(self, cr, uid, context=None):
        ctx = dict(context or {}, account_period_prefer_normal=True)
        period_ids = self.pool.get('account.period').find(cr, uid, context=ctx)
        return period_ids and period_ids[0] or False

    _columns = {
        'period_id': fields.many2one('account.period', u'会计期间', required=True),
    }
    _defaults = {
        'period_id': _get_period,
    }

    # 快速创建凭证明细
    def fast_create(self, cr, uid, ids, vals):

        try:
            debit = vals.get('debit', 0)
            credit = vals.get('credit', 0)
            analytic_account_id = vals.get('analytic_account_id', 0)
            vals.update({'company_id': 1, 'state': 'valid', 'fz_journal_id': 1})
            if analytic_account_id:
                cr.execute("""
                    insert into account_move_line
                    (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,analytic_account_id,state) values
                    ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s','%(date_maturity)s',
                    '%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(analytic_account_id)s','%(state)s')
                    returning id;""" % (vals))
                line_id = cr.fetchall()
                if not line_id:
                    raise osv.except_osv(_('错误'), _(' 操作失败,请重试'))
                line_id = line_id[0][0]
                vals.update({'line_id': line_id, 'user_id': uid, 'amount': credit - debit, 'unit_amount': 0, 'fz_journal_id': 1})
                cr.execute("""
                    insert into account_analytic_line (move_id,name,general_account_id,user_id,date,account_id,journal_id,amount,unit_amount) values
                    ('%(line_id)s','%(name)s','%(account_id)s','%(user_id)s','%(date)s','%(analytic_account_id)s','%(fz_journal_id)s','%(amount)s','%(unit_amount)s')
                    returning id;""" % (vals))
            else:
                cr.execute("""insert into account_move_line
                (name,centralisation,account_id,move_id,journal_id,period_id,date,date_maturity,credit,debit,ref,company_id,state) values
                ('%(name)s','%(centralisation)s','%(account_id)s','%(move_id)s','%(journal_id)s','%(period_id)s','%(date)s',
                '%(date_maturity)s','%(credit)s','%(debit)s','%(ref)s','%(company_id)s','%(state)s')
                returning id;""" % (vals))
        except Exception as e:
            cr.rollback()
            import traceback
            traceback.print_exc()
            _logger.error(u"凭证明细生成过程中错误: %s" % e)
        return True

    # 生成月末价之前的检查及获得参数

    def check_ok(self, cr, uid, ids, period_instance, context=None):

        sql = """select id from monthly_price where period_id=%s""" % (period_instance.id)
        cr.execute(sql)
        sql_standard = cr.fetchall()

        if sql_standard:
            raise osv.except_osv(_('错误!'), _("本月月末价格已生成 如需重新计算 请先删除!"))

        if period_instance.state == 'done':
            raise osv.except_osv(_('错误!'), _("当前会计期间已经关账!"))
        # 计算日
        use_date = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')

        # ---------------------------------------------------------
        start_time = use_date + " 16:00:00"  # 本月月初时间
        end_time = period_instance.date_stop + " 15:59:59"  # 本月月末时间
        # ---------------------------------------------------------
        # 上月最后一天
        lastdate = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        # ---------------------------------------------------------
        # 上一个月
        last_period = self.pool.get('account.period').search(cr, uid, [('date_stop', '=', lastdate), ('special', '!=', True)])[0]

        # ---------------------------------------------------------
        if not last_period:
            raise osv.except_osv(u'警告', u'没有找到上一个会计期间')

        sql_product = """select id from product_product where calc_ok='t'"""
        cr.execute(sql_product)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的产品')
        product_ids = tuple([x[0] for x in fet_ids] + [0])

        sql_purchase_location = """select id from stock_location where calc_ok='t' and type='in'"""
        cr.execute(sql_purchase_location)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的采购库位')
        in_locations = tuple([x[0] for x in fet_ids] + [0])

        sql_base_location = """select id from stock_location where calc_ok='t' and type='base'"""
        cr.execute(sql_base_location)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的基础库位')
        base_locations = tuple([x[0] for x in fet_ids] + [0])

        return product_ids, in_locations, base_locations, period_instance.id, last_period, start_time, end_time

    # ###################################
    # 优化生成月末价格
    # ###################################

    def price_get(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        wiz_obj = self.browse(cr, uid, ids[0])

        # 会计期间
        period_instance = wiz_obj.period_id

        # 检查期间并获得参数
        product_ids, in_locations, base_locations, period_id, last_period, start_time, end_time = self.check_ok(cr, uid, ids, period_instance, context)

        dic = {'start_time': start_time, 'end_time': end_time, 'last_period': last_period, 'period_id': period_id,
             'in_locations': in_locations, 'base_locations': base_locations, 'product_ids': product_ids}

        # ##插入月末价格;不含月末库存
        error = False
        try:
            sql = """
            insert into monthly_price (location_id,product_id,period_id,qty_in,price_in,qty_end,price)
                (
                ------------------------------------------------------
                ---外购入库按产品,按库位汇总
                ------------------------------------------------------
                with data_in as
                (
                    select product_id,location_id,round(sum(qty),5) as qty,sum(amount) as amount
                    from

                        (-----采购入库数量,单价
                        select product_id,a.location_dest_id as location_id,coalesce(sum(a.product_qty*uom2.factor/uom1.factor),0) as qty,
                        sum(coalesce(a.price_unit,0)*a.product_qty) as amount
                        from stock_move a

                        left join product_uom uom1 on uom1.id=a.product_uom
                        left join product_product pp on pp.id=a.product_id
                        left join product_template pt on pp.product_tmpl_id=pt.id
                        left join product_uom uom2 on uom2.id=pt.uom_id

                        ----- 从采购类别库位到基础类别库位
                        where a.location_id in %(in_locations)s and a.location_dest_id in %(base_locations)s and a.state='done'
                            and product_id in %(product_ids)s
                            and a.date>='%(start_time)s' and a.date<='%(end_time)s'
                            group by a.product_id,a.location_dest_id,a.price_unit

                         -----采购入库数量,单价###退货
                        union all
                            select product_id,a.location_id as location_id,coalesce(-sum(a.product_qty*uom2.factor/uom1.factor),0) as qty,
                            coalesce( -sum(coalesce(a.price_unit,0)*a.product_qty),0) as amount
                            from stock_move a
                            left join product_uom uom1 on uom1.id=a.product_uom
                            left join product_product pp on pp.id=a.product_id
                            left join product_template pt on pp.product_tmpl_id=pt.id
                            left join product_uom uom2 on uom2.id=pt.uom_id

                            ------ 从基础类别库位到采购类别库位
                            where a.location_dest_id in %(in_locations)s and a.location_id in %(base_locations)s and a.state='done'
                            and product_id in %(product_ids)s
                            and a.date>='%(start_time)s' and a.date<='%(end_time)s'
                            group by a.product_id,a.location_id,a.price_unit)  a group by product_id,location_id ),

                ---取的上一个月的库存总数 按产品
                data_last as (
                    select  b.product_id,coalesce(sum(b.qty_end),0) as qty,coalesce(sum(b.qty_end*b.price),0) as amount
                    from product_product a
                    left join monthly_price b on (b.product_id=a.id and period_id=%(last_period)s)
                    where  a.calc_ok='t' group by b.product_id
                ),

                ----本月采购入库按产品汇总
                data_current as (
                    select b.product_id,coalesce(sum(b.qty),0) as qty,coalesce(sum(b.amount),0) as amount
                    from product_product a
                    left join data_in b on (b.product_id=a.id)
                    where  a.calc_ok='t' group by b.product_id
                ),
                ----本月新增数量 按产品 按库位
                data_fs as (
                    select product_id,location_id,sum(qty) as qty from (
                        select product_id,a.location_dest_id as location_id,coalesce(sum(a.product_qty*uom2.factor/uom1.factor),0) as qty
                        from stock_move a
                        left join product_uom uom1 on uom1.id=a.product_uom
                        left join product_product pp on pp.id=a.product_id
                        left join product_template pt on pp.product_tmpl_id=pt.id
                        left join product_uom uom2 on uom2.id=pt.uom_id

                        --- 进入基础类倍库位
                        where a.location_dest_id in %(base_locations)s and a.state='done'
                        and product_id in %(product_ids)s
                        and a.date>='%(start_time)s' and a.date<='%(end_time)s'
                        group by a.product_id,a.location_dest_id

                    union all

                        select a.product_id,a.location_id as location_id,coalesce(-sum(a.product_qty*uom2.factor/uom1.factor),0) as qty
                        from stock_move a
                        left join product_uom uom1 on uom1.id=a.product_uom
                        left join product_product pp on pp.id=a.product_id
                        left join product_template pt on pp.product_tmpl_id=pt.id
                        left join product_uom uom2 on uom2.id=pt.uom_id

                        --- 流出基础库位

                        where a.location_id in %(base_locations)s and a.state='done'
                        and product_id in %(product_ids)s
                        and a.date>='%(start_time)s' and a.date<='%(end_time)s'
                        group by a.product_id,a.location_id) a group by a.product_id,a.location_id
                )
                ---主表查询
                select b.id as locatin_id,a.id as product_id,%(period_id)s as period_id,
                    coalesce(c.qty,0) as qty_in,

                    --入库均价
                    case when coalesce(f.qty,0)=0 then 0
                    else f.amount/f.qty end as price_in,
                    coalesce(g.qty,0)+coalesce(d.qty_end,0) as qty_end,

                    --月末价
                    case when coalesce(e.qty,0)+coalesce(f.qty,0)=0 then 0
                    else (coalesce(e.amount,0)+coalesce(f.amount,0))/(coalesce(e.qty,0)+coalesce(f.qty,0)) end as price
                    from product_product a

                    full join stock_location b on b.calc_ok='t' and b.type='base'
                    left join data_in c on
                             (c.location_id=b.id  and c.product_id=a.id)
                    left join monthly_price d on
                             (d.product_id=a.id and d.location_id=b.id
                              and d.period_id=%(last_period)s)
                    left join data_last e on e.product_id=a.id
                    left join data_current f on f.product_id=a.id
                    left join data_fs g on g.product_id=a.id and g.location_id=b.id
                    where a.calc_ok='t')
               """ % dic

            _logger.warning(u"================开始执行sql:%s" % time.strftime('%Y-%m-%d %H:%M:%S'))
            cr.execute(sql)
            _logger.warning(u"================采购入库结束sql:%s" % time.strftime('%Y-%m-%d %H:%M:%S'))

            mod_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            view = mod_obj.get_object_reference(cr, uid, 'weighting_average_price', 'action_monthly_price')
            view_id = view and view[1] or False
            result = act_obj.read(cr, uid, [view_id])[0]
            result['domain'] = [('period_id', '=', period_id)]
        except Exception as e:
            cr.rollback()
            error = True
            _logger.error("月末价格生成过程中错误: %s" % e)
        cr.commit()

        if error:
            raise osv.except_osv(u'警告', u'月末凭证生成过程中错误: %s' % e)

        return result

    def check_ok2(self, cr, uid, ids, period_instance, context=None):
        sql = """select id from monthly_price where period_id=%s""" % (period_instance.id)
        cr.execute(sql)
        sql_standard = cr.fetchall()
        if not sql_standard:
            raise osv.except_osv(_('错误!'), _("本月月末价格还未生成!"))
        if period_instance.state == 'done':
            raise osv.except_osv(_('错误!'), _("当前会计期间已经关账!"))
        # 计算日
        use_date = (datetime.datetime.strptime(period_instance.date_start, '%Y-%m-%d') + timedelta(days=-1)).strftime('%Y-%m-%d')
        # ---------------------------------------------------------
        start_time = use_date + " 16:00:00"  # 本月月初时间
        end_time = period_instance.date_stop + " 15:59:59"  # 本月月末时间
        # ---------------------------------------------------------

        sql_product = """select id from product_product where calc_ok='t'"""
        cr.execute(sql_product)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的产品')
        product_ids = tuple([x[0] for x in fet_ids] + [0])
        sql_out_location = """select id from stock_location where calc_ok='t' and type='out'"""
        cr.execute(sql_out_location)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的出库库位')
        out_locations = tuple([x[0] for x in fet_ids] + [0])
        sql = """select name from stock_location where id in %s and account_id is null""" % (tuple(out_locations),)
        cr.execute(sql)
        error_names = cr.fetchall()
        if error_names:
            nn = ''
            for each in error_names:
                nn += ' %s' % each[0]
            raise osv.except_osv(u'警告', u'请维护以下出库库位对应的会计科目:' + nn)
        sql_base_location = """select id from stock_location where calc_ok='t' and type='base'"""
        cr.execute(sql_base_location)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请至少配置一个参与核算为真的基础库位')
        base_locations = tuple([x[0] for x in fet_ids] + [0])
        sql_account = """select id from account_account where calc_ok='t' and active='t'"""
        cr.execute(sql_account)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请配置一个进销存库存科目为真的会计科目')
        if len(fet_ids) > 1:
            raise osv.except_osv(u'警告', u'请保证只有一个进销存库存科目为真的会计科目')
        account_id = fet_ids[0][0]
        sql_account = """select id from account_journal where calc_ok='t'"""
        cr.execute(sql_account)
        fet_ids = cr.fetchall()
        if not fet_ids:
            raise osv.except_osv(u'警告', u'请配置一个进销存账簿为真的会计科目')
        if len(fet_ids) > 1:
            raise osv.except_osv(u'警告', u'请保证只有一个进销存账簿为真的会计科目')
        journal_id = fet_ids[0][0]
        return product_ids, out_locations, base_locations, period_instance.id, start_time, end_time, account_id, journal_id

    # ###################################
    # 优化生成月末凭证
    # ###################################
    def journal_get(self, cr, uid, ids, context=None):

        if context is None:
            context = {}

        wiz_obj = self.browse(cr, uid, ids[0])

        # 会计期间
        period_instance = wiz_obj.period_id

        # 检查期间并获得参数
        product_ids, out_locations, base_locations, period_id, start_time, end_time, account_id2, journal_id2 = self.check_ok2(cr, uid, ids, period_instance, context)

        dic = {'start_time': start_time, 'end_time': end_time, 'period_id': period_id,
               'out_locations': out_locations, 'base_locations': base_locations, 'product_ids': product_ids}

        print(" ******************dic ***************")
        print(dic)

        error = False

        try:
            sql = """

            with data as (
            select product_id,location_id,location_dest_id,sum(qty) as qty from
                (
                ----- 出货
                select product_id,location_id,location_dest_id,coalesce(sum(a.product_qty*uom2.factor/uom1.factor),0) as qty from
                stock_move a
                    left join product_uom uom1 on uom1.id=a.product_uom
                    left join product_product pp on pp.id=a.product_id
                    left join product_template pt on pp.product_tmpl_id=pt.id
                    left join product_uom uom2 on uom2.id=pt.uom_id

                ---- 从基础库位到出库类别
                where a.location_id in %(base_locations)s and a.location_dest_id in %(out_locations)s
                    and  a.date>='%(start_time)s' and a.date<='%(end_time)s'
                    group by a.location_id,a.location_dest_id,a.product_id
                union all
                --- 退货
                select product_id,location_dest_id as location_id ,location_id as location_dest_id ,coalesce(-sum(a.product_qty*uom2.factor/uom1.factor),0) as qty from
                stock_move a
                    left join product_uom uom1 on uom1.id=a.product_uom
                    left join product_product pp on pp.id=a.product_id
                    left join product_template pt on pp.product_tmpl_id=pt.id
                    left join product_uom uom2 on uom2.id=pt.uom_id

                ---- 从出库类别到基础类别
                where a.location_dest_id in %(base_locations)s and a.location_id in %(out_locations)s
                and  a.date>='%(start_time)s' and a.date<='%(end_time)s'
                group by a.location_id,a.location_dest_id,a.product_id
                ) a

                group by  a.location_id,a.location_dest_id,a.product_id
            ),

            month_price as
                (select product_id,avg(price) as price from monthly_price
                where period_id=%(period_id)s group by product_id)

            ---主查询
            select a.location_id,a.location_dest_id,round(coalesce(sum(d.price*a.qty),0),2) as amount,
                coalesce(b.account_id,c.account_id) as account_id,
                coalesce(b.analytic_id,c.analytic_id) as analytic_id,f.name,g.name from
                data  a
                --- 进销存配置库位已配置该源库位和对应库位
                left join location_set b on (a.location_id=b.location_id and a.location_dest_id=b.location_dest_id and b.active='t')
                left join stock_location c on (c.id=a.location_dest_id)
                left join stock_location g on g.id=c.location_id
                left join month_price d on d.product_id=a.product_id
                left join stock_location e on e.id=a.location_id
                left join stock_location f on f.id=e.location_id
                group by a.location_id,a.location_dest_id,b.account_id,c.account_id,b.analytic_id,c.analytic_id,f.name,g.name
                having(round(sum(d.price*a.qty),2))<>0
            """ % (dic)

            cr.execute(sql)
            data_res = cr.fetchall()

            list = {}
            for each in data_res:
                location_id, location_dest_id, amount, account_id, analytic_id, name, name2 = each
                if location_id not in list.keys():
                    list.update({location_id: {location_dest_id: [amount, account_id, analytic_id, name, name2]}})
                else:
                    list[location_id].update({location_dest_id: [amount, account_id, analytic_id, name, name2]})

            move_ids = []

            for k, v in list.items():

                name = v.values()[0][-2] + u'进销存' + period_instance.name

                sql = """select id from account_move where ref='%s' and period_id=%s""" % (name, period_id)
                cr.execute(sql)
                if cr.fetchall():
                    continue

                dic_move = {
                        'journal_id': journal_id2,
                        'ref': name,
                        'period_id': period_id,
                        'date': period_instance.date_stop,
                }
                move_id = self.pool.get('account.move').create(cr, uid, dic_move, context)

                t_amount = 0

                for line in v.values():
                    amount, account_id, analytic_id, name, name2 = line
                    analytic_id = analytic_id or False
                    amount = float('%.2f' % amount)
                    t_amount += amount
                    if amount >= 0:
                        debit, credit = amount, 0
                    else:
                        credit, debit = -amount, 0
                    line_dic = {
                        'name': name,
                        'centralisation': debit > 0 and 'debit' or 'credit',
                        'partner_id': False,
                        'account_id': account_id,
                        'move_id': move_id,
                        'analytic_account_id': analytic_id,
                        'journal_id': journal_id2,
                        'period_id': period_id,
                        'date': period_instance.date_stop,
                        'date_maturity': period_instance.date_stop,
                        'credit': credit,
                        'debit': debit,
                        'ref': name2,
                    }
                    self.fast_create(cr, uid, ids, line_dic)

                line_dic = {
                    'name': name,
                    'centralisation': debit > 0 and 'debit' or 'credit',
                    'partner_id': False,
                    'account_id': account_id2,
                    'move_id': move_id,
                    'analytic_account_id': analytic_id,
                    'journal_id': journal_id2,
                    'period_id': period_id,
                    'date': period_instance.date_stop,
                    'date_maturity': period_instance.date_stop,
                    'credit': t_amount > 0 and t_amount or 0,
                    'debit': t_amount < 0 and -t_amount or 0,
                    'ref': name2, }
                self.fast_create(cr, uid, ids, line_dic)
                move_ids.append(move_id)
            mod_obj = self.pool.get('ir.model.data')
            act_obj = self.pool.get('ir.actions.act_window')
            view = mod_obj.get_object_reference(cr, uid, 'account', 'action_move_journal_line')
            view_id = view and view[1] or False
            result = act_obj.read(cr, uid, [view_id])[0]
            result['domain'] = [('id', 'in', move_ids)]
        except Exception as e:
            cr.rollback()
            error = True
            _logger.error("月末凭证生成过程中错误: %s" % e)
        cr.commit()
        if error:
            raise osv.except_osv(u'警告', u'月末凭证生成过程中错误: %s' % e)
        return result

monthly_price_wizard()
