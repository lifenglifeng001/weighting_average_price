# -*- coding: utf-8 -*-

from openerp.osv import fields, osv

class location_set(osv.osv):
    _name = 'location.set'
    _description = u'进销存配置'
    _columns = {
        'name': fields.char(u'单号', size=128),
        'active': fields.boolean(u'可用性'),
        'location_id': fields.many2one('stock.location', u'源库位'),
        'location_dest_id': fields.many2one('stock.location', u'目标库位'),
        'account_id': fields.many2one('account.account', u'科目', help=u'生成进销存凭证时对应的科目'),
        'analytic_id': fields.many2one('account.analytic.account', u'辅助核算科目', help=u'生成进销存凭证时对应的辅助核算科目'),
        'description': fields.char(u'备注', size=128),
    }
    _defaults = {
        'active': False,
    }
location_set()

class stock_location(osv.osv):
    _inherit = 'stock.location'
    _type = [('base', '基础类别'), ('in', '采购类别'), ('out', '出库类别')]
    _columns = {
        'account_id': fields.many2one('account.account', u'科目', help='生成进销存凭证时对应的科目'),
        'analytic_id': fields.many2one('account.analytic.account', u'辅助核算科目', help='生成进销存凭证时对应的辅助核算科目'),
        'type': fields.selection(_type, u'类型'),
        'calc_ok': fields.boolean(u'参与核算', select=True),
    }
    _defaults = {
        'type': '',
        'calc_ok': False,
    }
stock_location()

class product_template(osv.osv):
    _inherit = 'product.template'

    _columns = {
        'calc_ok': fields.related('product_variant_ids', 'calc_ok', type='boolean', string='参与核算'),
    }

    _defaults = {
        'calc_ok': False,
    }
product_template()

class product_product(osv.osv):
    _inherit = 'product.product'

    _columns = {
        'calc_ok': fields.boolean('参与核算'),
    }

    _defaults = {
        'calc_ok': False,
    }
product_product()

class account_journal(osv.osv):
    _inherit = 'account.journal'
    _columns = {
        'calc_ok': fields.boolean('进销存账簿', select=True, help='只能一个账簿勾选'),
    }
    _defaults = {
        'calc_ok': False,
    }
account_journal()

class account_account(osv.osv):
    _inherit = 'account.account'
    _columns = {
        'calc_ok': fields.boolean('进销存库存科目', select=True, help='只能一个科目勾选,用于进销存生成凭证'),
    }
    _defaults = {
        'calc_ok': False,
    }
account_account()
